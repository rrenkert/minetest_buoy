buoy = {}
function buoy:register_buoy(name, def)
	minetest.register_entity(name, {
		hp_max = def.hp_max,
		physical = true,
		liquids_pointable = true,
		collisionbox = def.collisionbox,
		visual = def.visual,
		visual_size = def.visual_size,
		mesh = def.mesh,
		textures = def.textures,
		drawtype = def.drawtype,
		on_rightclick = def.on_rightclick,
		type = def.type,
		sounds = def.sounds,
	})
end

buoy:register_buoy("buoy:harbour_green", {
	type = "buoy",
	hp_max = 200,
	collisionbox = {-1, -0.5, -1, 1, 2,1},
	visual = "mesh",
	mesh = "buoy.x",
	armor = {fleshy=100},
	textures = {"buoy_harbour_green.png"},
	visual_size = {x=6,y=6},
	drawtype = "front",
		on_rightclick = function(self, puncher, time_from_last_punch, tool_capabilities, dir)
		self.object:remove()
		if puncher and puncher:is_player() then
			puncher:get_inventory():add_item("main", "buoy:harbour_green")
		end
	end,
})

minetest.register_craftitem("buoy:harbour_green", {
	description = "Green Harbour Buoy",
	inventory_image = "buoy_harbour_green_inv.png",
	liquids_pointable = true,
		on_place = function(itemstack, placer, pointed_thing)
			if pointed_thing.type ~= "node" then
				return
			end
			pointed_thing.under.y = pointed_thing.under.y + 1
			minetest.env:add_entity(pointed_thing.under, "buoy:harbour_green")
			itemstack:take_item()
			return itemstack
		end,
})

buoy:register_buoy("buoy:harbour_red", {
	type = "buoy",
	hp_max = 200,
	collisionbox = {-1, -0.5, -1, 1, 2,1},
	visual = "mesh",
	mesh = "buoy.x",
	armor = {fleshy=100},
	textures = {"buoy_harbour_red.png"},
	visual_size = {x=6,y=6},
	drawtype = "front",
		on_rightclick = function(self, puncher, time_from_last_punch, tool_capabilities, dir)
		self.object:remove()
		if puncher and puncher:is_player() then
			puncher:get_inventory():add_item("main", "buoy:harbour_red")
		end
	end,
})

minetest.register_craftitem("buoy:harbour_red", {
	description = "Green Harbour Buoy",
	inventory_image = "buoy_harbour_red_inv.png",
	liquids_pointable = true,
		on_place = function(itemstack, placer, pointed_thing)
			if pointed_thing.type ~= "node" then
				return
			end
			pointed_thing.under.y = pointed_thing.under.y + 1
			minetest.env:add_entity(pointed_thing.under, "buoy:harbour_red")
			itemstack:take_item()
			return itemstack
		end,
})

minetest.register_node("buoy:generic_orange", {
        drawtype = "nodebox",
	description = "Generic Orange Buoy",
	tiles = {"buoy_generic_orange.png"},
	paramtype = "light",
	light_source = 8,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375000,-0.375000,-0.500000,0.375000,0.375000,0.500000}, 
			{-0.500000,-0.375000,-0.375000,0.500000,0.375000,0.375000},
			{-0.375000,-0.500000,-0.375000,0.375000,0.500000,0.375000}, 
		},
	},
	is_ground_content = false,
	drop = "buoy:generic_orange",
	groups = {oddly_breakable_by_hand=2},
})

minetest.register_node("buoy:generic_white", {
        drawtype = "nodebox",
	description = "Generic White Buoy",
	tiles = {"buoy_generic_white.png"},
	paramtype = "light",
	light_source = 8,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375000,-0.375000,-0.500000,0.375000,0.375000,0.500000}, 
			{-0.500000,-0.375000,-0.375000,0.500000,0.375000,0.375000},
			{-0.375000,-0.500000,-0.375000,0.375000,0.500000,0.375000}, 
		},
	},
	is_ground_content = false,
	drop = "buoy:generic_white",
	groups = {oddly_breakable_by_hand=2},
})

minetest.register_node("buoy:generic_yellow", {
        drawtype = "nodebox",
	description = "Generic Yellow Buoy",
	tiles = {"buoy_generic_yellow.png"},
	paramtype = "light",
	light_source = 8,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375000,-0.375000,-0.500000,0.375000,0.375000,0.500000}, 
			{-0.500000,-0.375000,-0.375000,0.500000,0.375000,0.375000},
			{-0.375000,-0.500000,-0.375000,0.375000,0.500000,0.375000}, 
		},
	},
	is_ground_content = false,
	drop = "buoy:generic_yellow",
	groups = {oddly_breakable_by_hand=2},
})

minetest.register_craft({
	output = 'buoy:harbour_green',
	recipe = {
		{'dye:dark_green'},
		{'default:steel_ingot'},
		{'default:steelblock'},
	}
})

minetest.register_craft({
	output = 'buoy:harbour_red',
	recipe = {
		{'dye:red'},
		{'default:steel_ingot'},
		{'default:steelblock'},
	}
})

minetest.register_craft({
	output = 'buoy:generic_orange',
	recipe = {
		{'','dye:orange','',},
		{'dye:orange','','dye:orange'},
		{'','dye:orange',''},
	}
})

minetest.register_craft({
	output = 'buoy:generic_white',
	recipe = {
		{'','dye:white','',},
		{'dye:white','','dye:white'},
		{'','dye:white',''},
	}
})

minetest.register_craft({
	output = 'buoy:generic_yellow',
	recipe = {
		{'','dye:yellow','',},
		{'dye:yellow','','dye:yellow'},
		{'','dye:yellow',''},
	}
})
